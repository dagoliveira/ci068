# Circuitos Lógicos - CI068

## Professor

- [Daniel Alfonso Gonçalves de Oliveira](http://www.inf.ufpr.br/dagoliveira)
- Departamento de informática - sala 60

## Acompanhamento 2019/1
- [Notas e Frequência](./docs/acompanhamento-2019-1/2019-1-acompanhamento.md)
- [Trabalho 1](./docs/acompanhamento-2019-1/trabalho1.md)
- [Prova 2](./docs/acompanhamento-2019-1/2019-1-prova2.md)

## Informações Gerais
- [Slides das aulas (PDF)](./aulas)
- [Plano de aulas](./docs/plano-aulas-19-1.md)
- [Plano de aulas (PDF)](./docs/plano-aulas-19-1.pdf)
- Turma C: Terças e Quintas 13:30 - 15:10 sala PC-17

## Ferramenta para Trabalhos - Logisim

- [Página oficial](http://www.cburch.com/logisim/pt/)
- [Tutoriais e Documentação](http://www.cburch.com/logisim/pt/docs.html)
- [Link direto para Download, v 2.7.1](./logisim/logisim-generic-2.7.1.jar)
    - Como executar: java -jar logisim-generic-2.7.1.jar

## Trabalho 1
- [Enunciado trabalho 1](./trabalhos/trabalho1.md) [pdf](./trabalhos/trabalho1.pdf)


## Trabalho 2
- [Enunciado trabalho 2](./trabalhos/trabalho2.md) [pdf](./trabalhos/trabalho2.pdf)

## Avaliação

A avaliação será composta por duas provas (P1 e P2) e dois trabalhos (T1 e T2)

**Média Provas = (P1 + 2*P2)/3**

**Média Trabalhos = (T1 + T2)/2**

**Média = (2*Média Provas + Média Trabalhos)/3**

Não haverá provas substitutivas, a menos nos casos previstos na resolução 37/97.

Será aprovado o aluno que tiver frequência mínima de 75% e média igual ou superior a 70: (frequência >= 0.75) and (média >= 70)

Será reprovado quem não tiver frequência mínima de 75% ou média menor do que 40: (frequência < 0.75) or (média < 40)

### Exame - Prova Final

Alunos com frequência mínima e média entre 40 e 69 poderão fazer uma prova final:  (frequência >= 0.75) and (média >= 40) and (média < 70).

Média final = (Média + Prova Final) / 2

Será aprovado o aluno que fizer a prova final e obtiver uma média final maior ou igual a 50: (Média final >= 50)


## Frequência mínima

Por ser um curso de 60 horas/aula, o aluno que faltar mais de 25% (15 horas/aula) será reprovado, ou seja, faltar 8 dias letivos ou mais estará reprovado


## Ementa

Sistemas de numeração. Aritmética binária. Minimização e decomposição de funções booleanas. Circuitos combinacionais Circuitos sequenciais. Máquinas de estados.
- [Ementa Ficha 1](./docs/ementa-ficha1.pdf)

## Programa

1. Sistemas de numeração, conversão de bases.
2. Aritmética binária: Soma, Subtração, Multiplicação.
3. Equações booleanas, simplificação de álgebra booleana.
4. Mapas de Karnaugh.
5. Portas lógicas básicas.
6. Blocos combinacionais: multiplexadores, de-multiplexadores, decodificadores e seletores.
7. Latches e flip-flops.
8. Contadores síncronos e assíncronos.
9. Máquinas de estado finito: projeto, codificação de estados, fatoração.

## Bibliografia Básica

[1] T. Floyd. Sistemas Digitais: Fundamentos e Aplicações. Bookman, 2009. ISBN : 9788577801077.

[2] David Harris e Sarah Harris. Digital design and computer architecture. Morgan Kaufmann, 2007. ISBN : 9780123704979.

[3] M. Rabaey Jan, Chandrakasan Anantha e Nikolic Borivoje. Digital integrated circuits: a design perspective. Vol. 2. Pearson Education, 2003. ISBN : 0-13-090996-3.

[4] V. Pedroni. Eletronica Digital Moderna E Vhdl. Elsevier, 2010. ISBN : 9788535234657.

## Bibliografia Complementar

[5] F.G. Capuano. Sistemas Digitais: circuitos combinacionais e sequenciais. Érica, 2014. ISBN : 9788536506289.

[6] David Harris e Sarah Harris. Digital design and computer architecture. Vol. 2. Morgan Kaufmann, 2013. ISBN : 9780123944245.

[7] Elliott Mendelson. Algebra booleana e circuitos de chaveamento : resumo da teoria, 150 problemas resolvidos. McGraw-Hill, 1977.

[8] T.C. Pimenta. Circuitos Digitais: Análise e Síntese Lógica: Aplicações em FPGA. Elsevier Brasil, 2017. ISBN : 9788535266030.

[9] Herbert Taub. Circuitos digitais e microprocessadores. McGraw-Hill, 1984.

[10] João Antonio Zuffo. Subsistemas digitais e circuitos de pulso. Vol. 3. E. Blucher, 1980.
