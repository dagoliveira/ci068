---
geometry: margin=1in
papersize: a4
colorlinks: true
---

# Trabalho 1

# CI068 - Circuitos Lógicos - Prof. Daniel Oliveira


## Descrição Básica:

Com o simulador [Logisim](http://www.cburch.com/logisim/pt/index.html) utilizando basicamente portas lógicas e um display, implemente um subtrator de 2 números binários A e B de 4 dígitos cada.
A e B devem ser tratados como números positivos sem sinal obtendo um resultado R.

O resultado R da soma deverá ser mostrado em um display de 7 segmentos representado na base decimal.

Caso o resultado R for R<-9 ou R>9, escreva "-" no display. 
Acenda o ponto do display (ou led adicional) para indicar números negativos.

Casos não tratados no enunciado deverão ser discutidos com o professor.

## Regras Gerais:

O trabalho poderá ser feito em duplas.

A entrega será feita por e-mail com dois arquivos em anexo:

1. Relatório em PDF contendo o nome dos participantes do grupo e também:
    - Mapas de karnaugh
    - Equações booleanas simplificadas

2. Circuito funcional no Logisim

Trabalhos devem ser entregues até o dia 25/04/2019. Trabalhos entregues em atraso perderão dois pontos por dia de atraso.

**Atenção**: o circuito deve ser feito pelos alunos do grupo. O uso da ferramenta de análise combinacional dentro do Logisim ou cópia do trabalho (plágio), acarretará em nota igual a Zero para todos os envolvidos.
