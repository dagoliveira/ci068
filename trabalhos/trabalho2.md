---
geometry: margin=1in
papersize: a4
colorlinks: true
---

# Trabalho 2

# CI068 - Circuitos Lógicos - Prof. Daniel Oliveira


## Descrição Básica:

Com o simulador [Logisim](http://www.cburch.com/logisim/pt/index.html) utilizando basicamente portas lógicas, pinos de entrada e leds de indicação de saída, implemente uma máquina de vender café.

\centering

![](./coffee-machine.png){width=10% }\

\raggedright

A máquina irá aceitar apenas moedas de R\$0,50 e R\$1,00 e servirá dois tipos de café, Expresso (R\$0,50) e Latte (R\$1,00).

Internamente o circuito terá 5 entradas:

1. 50 Centavos (50C) – Indica que o usuário inseriu 50 centavos.
2. 100 Centavos (100C) – Indica que o usuário inseriu 1 real.
3. Botão  Expresso (BE) – Indica que o usuário escolheu o café Expresso.
4. Botão  Latte (BL) – Indica que o usuário escolheu o café Latte.
5. Botão  Cancelar (BC) – Indica que o usuário quer cancelar a operação (devolver dinheiro).

O circuito contará com 5 saídas para controlar os atuadores (motores/demais circuitos):

1. Troco 50 (T50) – A máquina deve liberar no troco uma moeda de 50 centavos.
2. Troco 100 (T100) – A máquina deve liberar no troco uma moeda de 1 real.
3. Liberar  Expresso (LE) – Indica que a máquina deve liberar o café Expresso.
4. Liberar  Latte (LL) – Indica que a máquina deve liberar o café Latte.
5. Ignorar Moedas (IM) – Indica que a máquina não deve aceitar mais moedas.

A máquina de estados finita (FSM) a ser implementada deverá ser a de Moore, e o circuito deve ser todo síncrono e executar em uma frequência de 10Hz (1 oscilações por segundo). Para cada transição deverá estar indicado todas as entradas (0 ou 1), e para cada estado, todas as saídas devem estar especificadas (0 ou 1).

Casos não tratados no enunciado deverão ser discutidos com o professor.

## Regras Gerais:

O trabalho poderá ser feito em duplas.

A entrega será feita por e-mail com dois arquivos em anexo:

1. Relatório em PDF contendo o nome dos participantes do grupo e a tabela de transição de estados
2. Relatório em papel contendo a Máquina de Estados Finito (Moore)
3. Circuito funcional no Logisim

Trabalhos devem ser entregues até o dia 11/06/2019. **Trabalhos entregues em atraso não serão considerados**.

**Atenção**: o circuito deve ser feito pelos alunos do grupo. O uso da ferramenta de análise combinacional dentro do Logisim ou cópia do trabalho (plágio), acarretará em nota igual a Zero para todos os envolvidos.
