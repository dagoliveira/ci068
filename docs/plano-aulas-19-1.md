---
author: 'Daniel Oliveira'
title: 'Plano de aulas - Circuitos Turma C - 2019/1'
lang: pt-br
geometry: margin=1.5cm
papersize: a4
---

| Data       | Descrição                                                                                       |
|------------|-------------------------------------------------------------------------------------------------|
| 19/02/2019 | Recepção de calouros                                                                            |
| 21/02/2019 | Recepção de calouros                                                                            |
| 26/02/2019 | Introdução aos sistemas computacionais, organização de um computador                            |
| 28/02/2019 | Abstração, Von Neumann                                                                          |
| 05/03/2019 | Carnaval                                                                                        |
| 07/03/2019 | Sistema de numeração, base 10, base 2, base 8, base 16, números inteiros e fracionários         |
| 12/03/2019 | Sinal magnitude, complemento de 1, complemento de 2, overflow, aritmética (soma)                |
| 14/03/2019 | Aritmética binária (multiplicação)                                                              |
| 19/03/2019 | Álgebra booleana, tabelas verdades, propriedades algébricas                                     |
| 21/03/2019 | Fatoração lógica e formas canônicas                                                             |
| 26/03/2019 | Portas lógicas e conversão de funções para circuitos                                            |
| 28/03/2019 | Estudo de forma de onda, apresentação do Logisim - **TRABALHO 1**                               |
| 02/04/2019 | Mapas de Karnaugh e Bits don't care                                                             |
| 04/04/2019 | Blocos combinacionais básicos (somador e subtrator)                                             |
| 09/04/2019 | Exercícios                                                                                      |
| 11/04/2019 | **Aula Cancelada**                                                                              |
| 16/04/2019 | **PROVA 1**                                                                                     |
| 18/04/2019 | Blocos combinacionais básicos (decodificador e multiplexador)                                   |
| 23/04/2019 | Blocos combinacionais básicos (multiplicador, shifter, comparadores)                            |
| 25/04/2019 | Blocos combinacionais básicos (funções com mux, full-adder com half-adder) **Entrega Trabalho** |
| 30/04/2019 | Circuitos sequênciais (Latches: SR, RS, D) - **TRABALHO 2**                                     |
| 02/05/2019 | Circuitos sequênciais (Flip-Flops: SR, D)                                                       |
| 07/05/2019 | Circuitos sequênciais (Flip-Flops: JK)                                                          |
| 09/05/2019 | Máquina de estados finito (Moore e Mealy)                                                       |
| 14/05/2019 | Projetos com máquinas de estado finito                                                          |
| 16/05/2019 | Projetos de circuitos combinacionais e sequênciais                                              |
| 21/05/2019 | Projetos de circuitos combinacionais e sequênciais                                              |
| 23/05/2019 | **Entrega de trabalho (EaD)**                                                                   |
| 28/05/2019 | **Entrega de trabalho (EaD)**                                                                   |
| 04/06/2019 | Exercícios                                                                                      |
| 06/06/2019 | **PROVA 2**                                                                                     |
| 27/06/2019 | Exame (Prova final)                                                                             |


