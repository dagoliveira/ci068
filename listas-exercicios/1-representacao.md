---
geometry: margin=1in
papersize: a4
---
# Circuitos Digitais - CI068
## Lista sobre representação em outras bases

1. Faça as seguintes conversões de base:
    a. $(100111010101)_2 = (...)_8 = (...)_{10} = (...)_{16}$
    b. $(D4)_{16} = (...)_2 = (...)_{10}$
    c. $(1001,1010)_2 = (...)_{10}$
    d. $(17,625)_{10} = (...)_{2}$

2. Quantos bits são necessários para representar os seguintes números usando notação binária sem sinal.
    a. 1024
    b. 2048
    c. 4096
    d. 16000
    e. 34000

3. Qual o maior e menor número representável utilizando 8 bits para as seguintes representações:
    a. Sinal-Magnitude?
    b. Complemento de 1?
    c. Complemento de 2?

4. Qual o maior e menor número representável utilizando 12 bits para as seguintes representações:
    a. Sinal-Magnitude?
    b. Complemento de 1?
    c. Complemento de 2?

5. Transforme os seguintes números representados com complemento de 2 de 6 bits para 10 bits.
    a. 011101
    b. 100011
    c. 010101
    d. 111111
    e. 100101

6. Transforme os seguintes números representados com Sinal-Magnitude de 6 bits para 10 bits.
    a. 011101
    b. 100011
    c. 010101
    d. 111111
    e. 100101

7. Faça as seguintes operações em binário, utilize a notação que achar mais apropriada (Sinal-Magnitude, Complemento de 1 ou Complemento de 2 ) e justifique:
    a. +12 - 7
    b. -24 - 10
    c. -15 + 8
