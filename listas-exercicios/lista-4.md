---
geometry: margin=1in
papersize: a4
header-includes:
     - \usepackage{longtable,booktabs}
---

# Circuitos Digitais - CI068

## Utilizando blocos básicos

#. Faça um somador completo de 32 bits usando apenas somadores de 4 bits (considere uma uma caixa fechada com 8 entradas e 4 saídas, alem da entrada do vai-um e saída de vai-um).

#. Construa um circuito que recebe 8 bits de entrada (dois números de 4 bits sem sinal) e um bit para indicar qual operação deve ser feita "OP". O circuito deve ter 4 saídas. (obs.: pode-se usar somadores completos e multiplexadores)
    - Caso a operação OP for '0': deve ser feita uma soma entre os dois números entrados
    - Caso a operação OP for '1': o primeiro número de 4 bits deve ser incremento de um (somado o valor um) e o segundo número deve ser ignorado 

#. Construa um multiplexador de 16X1 (16 entradas e uma saída, ou seja, 4 bits de seleção) usando apenas multiplexadores de 4X1.

## Funções


#. Implemente usando multiplexador uma porta XOR de 4 entradas

#. Implemente usando multiplexador uma porta NAND de 3 entradas

#. Implemente a seguinte função usando um multiplexador 8X1: $F = ABCD + \overline{A}.B.\overline{C}.\overline{D} + \overline{A}.\overline{B}.\overline{C} + A.\overline{B}.\overline{C}.D + A.B.\overline{C}.\overline{D} + \overline{A}.\overline{B}.C.\overline{D}$

#. Implemente a função descrita pela tabela abaixo usando um multiplexador 8X1 (oito entradas e uma saída, ou seja, 3 bits para seleção).

```{=latex}
\begin{longtable}[c]{llll|l}
\toprule
\multicolumn{4}{c|}{Entradas} & \multicolumn{1}{c}{Saída} \tabularnewline
\(A_3\) & \(A_2\) & \(A_1\) & \(A_0\) & \(F\) \tabularnewline
\midrule
\endhead
0 & 0 & 0 & 0 & 0  \tabularnewline
0 & 0 & 0 & 1 & 0  \tabularnewline
0 & 0 & 1 & 0 & 1  \tabularnewline
0 & 0 & 1 & 1 & 1  \tabularnewline
0 & 1 & 0 & 0 & 0  \tabularnewline
0 & 1 & 0 & 1 & 0  \tabularnewline
0 & 1 & 1 & 0 & 1  \tabularnewline
0 & 1 & 1 & 1 & 1  \tabularnewline
1 & 0 & 0 & 0 & 1  \tabularnewline
1 & 0 & 0 & 1 & 0  \tabularnewline
1 & 0 & 1 & 0 & 1  \tabularnewline
1 & 0 & 1 & 1 & 1  \tabularnewline
1 & 1 & 0 & 0 & 0  \tabularnewline
1 & 1 & 0 & 1 & 1  \tabularnewline
1 & 1 & 1 & 0 & 0  \tabularnewline
1 & 1 & 1 & 1 & 1  \tabularnewline
\bottomrule
\end{longtable}
```


## Contadores e Máquinas de Estado

#. Faça um contador irregular síncrono com a seguinte contagem: 0-2-4-7
    a. Usando flip-flops D
    b. Usando flip-flops J-K

#. Faça um contador irregular síncrono crescente/decrescente com a seguinte contagem: 1-2-3-5-6
    a. Usando flip-flops D
    b. Usando flip-flops J-K

#. Faça um circuito para identificar o seguinte padrão '010'. Considere que o circuito recebe um bit de entrada a cada clock e a saída deve ser '0' caso não seja o padrão e '1' caso identificou o padrão.
    - Exemplo, na seguinte entrada o padrão aparece 3 vezes
        - Entrada: 00010101111010000111
        - Saída: \ \ \ \ 00001010000001000000

    a. Faça o circuito usando máquina de Moore.
    b. Faça o circuito usando máquina de Mealy.


#. Faça um circuito para identificar o padrão '1101'.
    a. Usando máquina de Moore.
    b. Usando máquina de Mealy.

#. Construa um cadeado eletrônico com três botões: Reset 'R', '0' e '1'. Para destravar o cadeado é necessário apertar o Reset e em seguida entrar com o código '0010'. A saída do circuito em nível alto '1' destrava o cadeado.
